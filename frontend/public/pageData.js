const pgTitle = document.getElementById('title');
const classCodeInput = document.getElementById('codeInput');
const classNumInput = document.getElementById('numInput');
const classNumsDL = document.getElementById('classNums');
let classData = {};

const setTitle = () => {
	fetch('/title').then(res => res.text())
	.then(r => pgTitle.innerHTML = r)
	.catch(err => console.error(err));
};

const addToDataList = (lst, datalist) => {
	lst.forEach(item => {
		const opt = document.createElement('option');
		opt.value = item;
		datalist.appendChild(opt);
	});
};

const updateClassNums = () => {
	//clear dl
	while (classNumsDL.firstChild) {
		classNumsDL.removeChild(classNumsDL.firstChild);
	}
	
	//read classCode from other input
	//get classData.classCode array, store in DL
	const classNums = classData[classCodeInput.value.toUpperCase()];
	if (classNums !== undefined) {
		addToDataList(classNums, classNumsDL);
	}
};

const main = async () => {
	setTitle();

	classData = await (await fetch('/classdata')).json();

	//set datalist for classCodes
	addToDataList(Object.keys(classData), document.getElementById('classCodes'));

	//updates classnum autocomplete on focus
	classNumInput.addEventListener('focus', updateClassNums);
};

//call main
main();
