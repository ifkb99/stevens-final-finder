const table = document.getElementById('classlist');

const tableToPlaintext = (evt) => {
	evt.preventDefault(); //stop form from submitting

	const clsDataArr = [];
	table.childNodes.forEach(clsRow => {
		const clsData = [].reduce.call(clsRow.childNodes, (dataArr, td) => dataArr.concat([td.firstChild.data]), []);
		clsData.pop(); //remove trash icon
		clsDataArr.push(clsData.join(' ')); //transform to string
	});

	window.open().document.write('<!DOCTYPE html><html><body><p>' + clsDataArr.join('<br>') + '</p></body></html>');
};

document.getElementById('plaintext').addEventListener('click', tableToPlaintext);
