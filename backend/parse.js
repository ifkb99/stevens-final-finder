const fs = require('fs');
const db = require('./db.js');

//used to create indexes by class codes
const classData = {};

const parseFileToDb = filename => {
	fs.readFile(filename, 'utf8', (err, res) => {
		if (err) throw err;
		db.insertParsedPdf(
			res.split('\n').filter(line => {
				const firstTwo = line.split(' ', 2);
				//all classcodes are between 1 to 3 characters
				if (firstTwo[0].length > 0 && firstTwo[0].length < 4) {
					if (classData[firstTwo[0]] === undefined) {
						classData[firstTwo[0]] = [firstTwo[1]];
					} else {
						classData[firstTwo[0]].push(firstTwo[1]);
					}
					return true;
				}
				return false;
			})
			.map(line => arrToJson(parseLine(line)))
		)
		//insert all classNums by classCode into db
		db.insert('classData', classData);
	});
}

//call the main func
parseFileToDb(process.argv[2]);

//@param {string} line A single line of the pdftotext file
//@return {Array.<string>}
const parseLine = line => {
	//line format: CourseName CourseNum&Section Date TimeStart AM/PM TimeEnd AM/PM BuildingList ProfFirst ProfLast
	//example: BIA 660A 5/13/2019 6:15 PM 9:15 PM P‐120 Theodoros Lappas

	//9 is the max amount of spaces in a line
	return parseHelper(line, 9, []);
}

const parseHelper = (line, spacesLeft, acc) => {
	if (line === "") { 
		//parser broke
		throw "xkcd.com/1421";
	} else if (spacesLeft === 1) {
		//the last space SHOULD be between prof first and last name
		return acc.concat(line);
	}
	
	//find next space
	let spcIdx = line.indexOf(' ');
	//spaces 4 and 6 are in between two parts of the time ie the space in (9:00 PM)
	if (spacesLeft === 4 || spacesLeft === 6) {
		//spcIdx = line.indexOf(' ', spcIdx+1);
		spcIdx += 3;
		spacesLeft--;
	}

	//break off one word at the space
	return parseHelper(line.slice(spcIdx+1), spacesLeft-1, acc.concat(line.slice(0, spcIdx)));
}

const arrToJson = parsedArr => {
	const data = {};
	data['_id'] = parsedArr[0] + ' ' + parsedArr[1];
	data.classCode = parsedArr[0];
	data.classNum = parsedArr[1];
	data.data = parsedArr;
	return data;
}

//program flow
//  Empty out db/delete old make new
//  parse pdf line by line to put into db
