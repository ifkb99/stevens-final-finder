const express = require('express');
const fs = require('fs');
const app = express();
const router = express.Router();

//my imports
const db = require('./db.js');

const port = 9001;

app.use('/', router);
app.use(express.static('public'));
router.use(express.json());

router.get('/', (req, res) => {
	res.sendFile(__dirname + '/public/index.html');
});

router.post('/classinfo', async (req, res) => {
	const classCode = req.body.classCode.toUpperCase();
	const classNum  = req.body.classNum.toUpperCase();

	res.setHeader('Content-Type', 'application/javascript');
	try {
		res.status(200);
		res.send(await db.getClassInfo(classCode, classNum));
	} catch (err) {
		//console.error(
			//'Error reading class data:', '\n',
			//'Class code:', classCode, '\n',
			//'Class num:', classNum, '\n',
			//'Error:', err
		//);
		res.status(400);
		res.send([classCode,  classNum]);
	}
});

router.get('/title', async (req, res) => {
	res.send(await db.getTitle());
});

router.get('/classdata', async (req, res) => {
	res.send(await db.getClassData());
});

app.listen(port);
console.log('Server running on port', port);
