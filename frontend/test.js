import test from 'ava';
const db = require('./db.js');

test('DB not empty', async t => {
	t.notDeepEqual(await db.readAll(), { total_rows: 0, offset: 0, rows: [] })
});
