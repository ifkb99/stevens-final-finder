const classdiv = document.getElementById('classlist');
const inputForm = document.getElementById('classEntry');
const errorBox = document.getElementById('error');

const displayErr = err => {
	errorBox.innerHTML = err;
}

const classInfoFromForm = inputs => [].reduce.call(inputs, (data, input) => {
	if (input.type !== 'submit') {
		data[input.name] = input.value;
	}
	return data;
}, {});

const reqClassFromDb = submit => {
	//body of response:
	//[classData]

	//stop default form post
	submit.preventDefault();

	const searchData = classInfoFromForm(inputForm.elements);
	if (searchData.classCode === '' || searchData.classNum === '') {
		displayErr('Search form must not be empty');
	} else {
		//await db response
		fetch('/classinfo', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify(searchData)
		})
		.then(res => parseResponse(res.status, res.json())) //pass class data to addClass function
		.catch(err => console.error(err));
	}
};

const removeClass = (e) => {
	//remove class from list when clicked
	const tableRow = e.target.parentNode.parentNode;
	tableRow.parentNode.removeChild(tableRow);
}

const parseResponse = async (stat, json) => {
	const resBody = await json;
	switch (stat) {
		case 200:
			addClass(resBody);
			break;
		case 400:
			displayErr(resBody.join(' ') + ' is not a valid request');
			break;
		default:
			displayErr("It's fucked");
	}
}

//takes in class array from res body
const addClass = dbRes => {
	//create class object from db response
	const clsBody = document.createElement('tr');

	//add items from response to body
	dbRes.forEach(value => {
		const clsData = document.createElement('td');
		clsData.innerHTML = value;
		clsBody.appendChild(clsData);
	});

	//add delete button to remove class
	const rmv = document.createElement('img');
	rmv.className = 'remove';
	rmv.src = 'trash.png';
	rmv.style.height = '32px';
	rmv.style.width = '32px';
	//add remove button
	rmv.addEventListener('click', removeClass);
	clsBody.appendChild(document.createElement('td')).appendChild(rmv);

	//attach body to classlist
	classdiv.appendChild(clsBody);

	//clear any errors
	displayErr('');
}

//hitting submit sends POST req
inputForm.addEventListener('submit', reqClassFromDb);
