const PDB = require('pouchdb');
const db = new PDB('http://couchdb:5984/finals');

/*
 *	Data format:
 *	{
 *		_id: classCode + classNum (with space)
 *		classCode: CS
 *		classNum: 385A
 *		data: [rest of data]
 *	}
 */

//db.changes().on('change', () => {
	//console.log('change made');
//});

//search by classcode and num
const get = async id => {
	try {
		return await db.get(id);
	} catch (err) {
		console.error(err);
	}
}

exports.readAll = async () => {
	return await db.allDocs({include_docs: true});
}

//TODO: query by just classcode/classnum

const insertOne = (id, key, val) => {
	const data = {};
	data['_id'] = id;
	data[key] = val;
	db.put(data, force=true)
		.catch(err => console.error('Insertion error:', err));
};


//put array of class data into db
exports.insertParsedPdf = parsedPdfArr => {
	db.bulkDocs(parsedPdfArr)
		.then(res => console.log('Insert success'))
		.catch(err => console.error('Error:', err));
};

exports.get = async id => {
	try {
		return await get(id);
	} catch (err) {
		console.error(err);
	}
};

exports.insert = (id, data) => insertOne(id, id, data);

exports.resetDb = () => {
	db.destroy(res => console.log(res));
};
