#!/bin/bash
URLFILE=/finalsgrabber/finalsurl

# get finals pdf url
ONLINEURL=$( curl -s https://www.stevens.edu/directory/office-registrar/final-exam-schedule \
		| grep -o 'https:[a-zA-Z0-9%&/:_\.]*.pdf' )

# see if urlfile exists, then compare urls
if [ ! -f $URLFILE ] || [ "$( cat $URLFILE )" != "$ONLINEURL" ]; then
	# need to redownload pdf
	curl -o /finalsgrabber/finals.pdf -s $ONLINEURL
	echo "$ONLINEURL" > $URLFILE
	echo 'Finals pdf downloaded!'
	bash /finalsgrabber/pdfToText.sh
else
	echo 'Finals pdf already up to date'
fi
