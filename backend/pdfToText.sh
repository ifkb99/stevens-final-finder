#!bin/bash
WORKDIR=/finalsgrabber
PDFFILE=$WORKDIR/finals.pdf
TXTFILE=$WORKDIR/pdf_to.txt

if [ -f "$PDFFILE" ]; then
	echo 'converting pdf to text file'
	pdftotext -raw -nopgbrk $PDFFILE $TXTFILE

	echo 'deleting old db'
	node $WORKDIR/resetDb.js

	echo 'parsing text file into new db' 
	node $WORKDIR/parse.js $TXTFILE

	# store pdf title in db
	node $WORKDIR/setData.js 'title' "$( head -2 $TXTFILE | tail -1 | cut -d ' ' -f 1,3 )"
	echo 'title updated'

	#echo 'deleting text and pdf files'
	rm $TXTFILE $PDFFILE
else
	echo "PDF file does not exist"
fi
