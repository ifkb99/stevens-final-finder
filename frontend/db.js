const PDB = require('pouchdb');
const db = new PDB('http://couchdb:5984/finals');

/*
 *	Data format:
 *	{
 *		_id: classCode + classNum (with space)
 *		classCode: CS
 *		classNum: 385A
 *		data: [rest of data]
 *	}
 */

//search by classcode and num
const get = async id => {
	try {
		return await db.get(id);
	} catch (err) {
		console.error(err);
	}
}

exports.readAll = async () => {
	return await db.allDocs({include_docs: true});
}

exports.getClassInfo = async (classCode, classNum) => {
	//only return class data array
	return (await get(classCode + ' ' + classNum)).data;
}

const insertOne = (id, key, val) => {
	const data = {};
	data['_id'] = id;
	data[key] = val;
	db.put(data)
		.catch(err => console.error(err));
};

exports.getTitle = async () => {
	return (await get('title')).title;
};

exports.getClassData = async () => {
	return (await get('classData')).classData;
};
